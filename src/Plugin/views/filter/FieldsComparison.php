<?php

/**
 * @file
 * Contains \Drupal\views\Plugin\views\filter\Combine.
 */

namespace Drupal\views_fields_comparison\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter handler which allows to filter by fields comparison.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("fields_comparison")
 */
class FieldsComparison extends FilterPluginBase {

  /**
   * @var views_plugin_query_default
   */
  var $query;

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['left_field'] = array('default' => '');
    $options['right_field'] = array('default' => '');

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $this->view->initStyle();

    // Allow to choose all fields as possible
    if ($this->view->style_plugin->usesFields()) {
      $options = array();
      foreach ($this->view->display_handler->getHandlers('field') as $name => $field) {
        // Only allow clickSortable fields. Fields without clickSorting will
        // probably break in the Combine filter.
        if ($field->clickSortable()) {
          $options[$name] = $field->adminLabel(TRUE);
        }
      }
      if ($options && count($options) >= 2) {
        $form['left_field'] = array(
          '#type' => 'select',
          '#title' => $this->t('Choose left field'),
          '#description' => $this->t("This filter doesn't work for very special field handlers."),
          '#options' => $options,
          '#default_value' => $this->options['left_field'],
        );
        $form['right_field'] = array(
          '#type' => 'select',
          '#title' => $this->t('Choose right field'),
          '#description' => $this->t("This filter doesn't work for very special field handlers."),
          '#options' => $options,
          '#default_value' => $this->options['right_field'],
        );
      }
      else {
        $form_state->setErrorByName('', $this->t('You have to add some fields to be able to use this filter.'));
      }
    }
  }

  function operators() {
    $operators = array(
      '<' => array(
        'title' => $this->t('Is less than'),
        'method' => 'opSimple',
        'short' => $this->t('<'),
        'values' => 1,
      ),
      '<=' => array(
        'title' => $this->t('Is less than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('<='),
        'values' => 1,
      ),
      '=' => array(
        'title' => $this->t('Is equal to'),
        'method' => 'opSimple',
        'short' => $this->t('='),
        'values' => 1,
      ),
      '!=' => array(
        'title' => $this->t('Is not equal to'),
        'method' => 'opSimple',
        'short' => $this->t('!='),
        'values' => 1,
      ),
      '>=' => array(
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('>='),
        'values' => 1,
      ),
      '>' => array(
        'title' => $this->t('Is greater than'),
        'method' => 'opSimple',
        'short' => $this->t('>'),
        'values' => 1,
      ),
    );

    return $operators;
  }

  public function operatorOptions($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  public function canExpose() {
    return FALSE;
  }
  
  protected function canBuildGroup() {
    return FALSE;
  }

  public function query() {
    $this->view->_build('field');
    $fields = array();
    // Only add the fields if they have a proper field and table alias.
    foreach ([
               $this->options['left_field'],
               $this->options['right_field']
             ] as $id) {
      // Overridden fields can lead to fields missing from a display that are
      // still set in the non-overridden combined filter.
      if (!isset($this->view->field[$id])) {
        // If fields are no longer available that are needed to filter by, make
        // sure no results are shown to prevent displaying more then intended.
        $this->view->build_info['fail'] = TRUE;
        continue;
      }
      $field = $this->view->field[$id];
      // Always add the table of the selected fields to be sure a table alias exists.
      $field->ensureMyTable();
      if (!empty($field->field_alias) && !empty($field->field_alias)) {
        $fields[] = "$field->tableAlias.$field->realField";
      }
    }
    if ($fields && count($fields) == 2) {
      $info = $this->operators();
      if (!empty($info[$this->operator]['method'])) {
        $this->{$info[$this->operator]['method']}($fields[0], $fields[1]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    $fields = $this->displayHandler->getHandlers('field');
    if ($this->displayHandler->usesFields()) {
      foreach ([
                 $this->options['left_field'],
                 $this->options['right_field']
               ] as $id) {
        if (!isset($fields[$id])) {
          // Combined field filter only works with fields that are in the field
          // settings.
          $errors[] = $this->t('Field %field set in %filter is not set in this display.', array(
            '%field' => $id,
            '%filter' => $this->adminLabel()
          ));
          break;
        }
        elseif (!$fields[$id]->clickSortable()) {
          // Combined field filter only works with simple fields. If the field is
          // not click sortable we can assume it is not a simple field.
          // @todo change this check to isComputed. See
          // https://www.drupal.org/node/2349465
          $errors[] = $this->t('Field %field set in %filter is not usable for this filter type. Fields comparison filter only works for simple fields.', array(
            '%field' => $fields[$id]->adminLabel(),
            '%filter' => $this->adminLabel()
          ));
        }
      }
    }
    else {
      $errors[] = $this->t('%display: %filter can only be used on displays that use fields.', array('%display' => $this->displayHandler->display['display_title'], '%filter' => $this->adminLabel()));
    }
    return $errors;
  }

  protected function opSimple($left_field, $right_field) {
    $this->query->addWhereExpression($this->options['group'], "$left_field {$this->operator} $right_field", array());
  }

}
