<?php

/**
 * @file
 * Provide views data.
 */


/**
 * Implements hook_views_data().
 */
function views_fields_comparison_views_data() {

  $data['views']['fields_comparison'] = array(
   'title' => t('Fields comparison'),
    'help' => t('Compare two database fields.'),
    'filter' => array(
      'id' => 'fields_comparison',
    ),
  );
  return $data;
}